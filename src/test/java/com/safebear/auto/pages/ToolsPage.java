package com.safebear.auto.pages;

import com.safebear.auto.pages.locators.ToolsPageLocators;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.openqa.selenium.WebDriver;

//Will ensure that the ToolsPage will ask for the 'driver'
@RequiredArgsConstructor
public class ToolsPage {

    //Link our ToolsPageLocators to this file
    ToolsPageLocators locators = new ToolsPageLocators();

    //Ensure that we have a 'Driver' (e.g. ChromeDriver or FirefoxDriver)
    @NonNull
    WebDriver driver;

    public String getPageTitle(){
        return driver.getTitle();
    }

    public String checkForLoginSuccessfulMessage()
    {
       return driver.findElement(locators.getSuccessfulLoginMessage()).getText();
    }
}

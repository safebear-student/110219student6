package com.safebear.auto.utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Utils {

    /**
     * Private = only for this class
     * Static = only one variable
     * Final = can't be changed after its value is set
     *
     * Name of the variable is in caps because it never changes
     *
     * System.getProperty looks at our CI tool and gets the URL for the test environment
     * The default test environment is the 'def' value
     */
    private static final String URL = System.getProperty("url", "http://toolslist.safebear.co.uk:8080");
    private static final String BROWSER = System.getProperty("browser", "chrome");

    /**
     * Getter for the URL
     *
     * "return the URL of my test environment / application
     */


    public static String getUrl() {
        return URL;
    }


    public static WebDriver getDriver() {


        //This tells selenium where our chromedriver is
        System.setProperty("webdriver.chrome.driver", "src/test/resources/drivers/chromedriver.exe");

        //This tells selenium where our firefoxdriver is
        System.setProperty("webdriver.firefox.driver", "src/test/resources/drivers/geckodriver.exe");

    //Here we're setting the size of our browser
        ChromeOptions options = new ChromeOptions();
        options.addArguments("window-size=1366,778");

        //This chooses the browser
        switch (BROWSER) {

            case "chrome":

                return new ChromeDriver(options);

            case "firefox":

                return new FirefoxDriver();
                default:
                    return new ChromeDriver(options);

            case "headless":
                options.addArguments("headless","disable-gpu");
                return new ChromeDriver(options);
        }
    }


}

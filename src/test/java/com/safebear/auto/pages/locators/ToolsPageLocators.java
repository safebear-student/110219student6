package com.safebear.auto.pages.locators;

import org.openqa.selenium.By;
import lombok.Data;


@Data
public class ToolsPageLocators {

    //Messages

    private By successfulLoginMessage = By.xpath(".//body/div[@class='container']/p/b");

}

package com.safebear.auto.pages.locators;

import lombok.Data;
import org.openqa.selenium.By;

@Data
public class LoginPageLocators {

    //Text input fields

    private By usernameLocator = By.id("username");
    private By passwordLocator = By.id("password");

    //Buttons
    private By buttonLocator = By.id("enter");

    //Warning messages
    private By failedLoginMessage = By.xpath(".//p[@id='rejectLogin']/b");
}

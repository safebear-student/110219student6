Feature: Search

  In order to find tasks
  As a user
  I want to see a task displayed in the list

  Rules:
  * The User must be informed if there are no results from the search
  * The User must see the search results listed if there is a matching criteria

  Scenario Outline: The one where a user searches for a task
    Given I am on the tasks list
    When I search for the following task '<searchCriteria>'
    Then I can see the following message: '<validationMessage>'
    Examples:
      | searchCriteria | validationMessage |
      | plymouth       | No results        |
      | test           | Results listed    |
package com.safebear.auto.pages;

import com.safebear.auto.utils.Utils;
import com.safebear.auto.pages.locators.LoginPageLocators;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.openqa.selenium.WebDriver;

//Will ensure that the LoginPage will ask for a 'driver'
@RequiredArgsConstructor
public class LoginPage {

    //Link our LoginPageLocators to this file
    LoginPageLocators locators = new LoginPageLocators();

    //Ensure that we have a 'driver' (e.g ChromeDriver or FirefoxDriver)
    @NonNull
    WebDriver driver;

    public String getPageTitle(){
        return driver.getTitle();
    }

    //Action commands

    //Enter text into the Username field
    public void enterUsername(String username){
        driver.findElement(locators.getUsernameLocator()).sendKeys(username);
    }


    //Enter text into the Password field
    public void enterPassword (String password){
        driver.findElement(locators.getPasswordLocator()).sendKeys(password);
    }

    //Click on Login button
    public void clickLoginButton(){
        driver.findElement(locators.getButtonLocator()).click();

    }

    public String checkForFailedLoginWarning()
    {
        return driver.findElement(locators.getFailedLoginMessage()).getText();
    }
}
